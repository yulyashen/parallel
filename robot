#include <iostream>
#include <thread>
#include <condition_variable>
#include <atomic>
#include <vector>
using namespace std;

//Робот на семафоре
class Semaphore {
private:
	atomic<int> count;
	condition_variable semaphoreSignal;
	mutex mtx;
public:
	explicit Semaphore() {
		count.store(1);
	}
	void wait() {
		unique_lock<mutex> lock(mtx);
		while (count.load() <= 0) {
			semaphoreSignal.wait(lock);
		}
		count--;
	}

	void signal() {
		lock_guard<mutex> lock(mtx);
		count.fetch_add(1);
		semaphoreSignal.notify_one();
	}
};
class SemaphoreRobot {
private:
	Semaphore semaphore;
public:
	explicit SemaphoreRobot() {}
	void rightStep() {
		semaphore.wait();
		this_thread::sleep_for(chrono::seconds(1));
		cout << "right" << endl;
		semaphore.signal();
		this_thread::sleep_for(chrono::seconds(1));
	}
	void leftStep() {
		semaphore.wait();
		this_thread::sleep_for(chrono::seconds(1));
		cout << "left" << endl;
		semaphore.signal();
		this_thread::sleep_for(chrono::seconds(1));
	}
};



//Робот на условных переменных
class ConditionalVariableRobot {
	condition_variable rightLeg;   
	condition_variable leftLeg;   
	atomic<int> currentLeg; 
	mutex mtx;
public:
	explicit ConditionalVariableRobot() {
		currentLeg.store(0);
	}
	void rightStep() {
		unique_lock<mutex> lock(mtx);
		while (currentLeg.load() == 1) {
			rightLeg.wait(lock);
		}
		this_thread::sleep_for(chrono::seconds(1));
		cout << "right" << endl;
		currentLeg.store(1);	
		leftLeg.notify_one();
	}
	void leftStep() {
		unique_lock<mutex> lock(mtx);
		while (currentLeg.load() == 0) {
			leftLeg.wait(lock);
		}
		this_thread::sleep_for(chrono::seconds(1));
		cout << "left" << endl;
		currentLeg.store(0);
		rightLeg.notify_one();
	}
};

//ConditionalVariableRobot Gosha;
SemaphoreRobot Gosha;

int main() {
	vector<thread> threads;
	threads.resize(2);
	threads[0] = thread([]() {
		int i = 0;
		while (true) {
			Gosha.leftStep();
		} 
	}); 
	threads[1] = thread([]() {
		int i = 0;
		while (true) {
			Gosha.rightStep();
		}
	});
	threads[0].join();
	threads[1].join();
	return 0;
}