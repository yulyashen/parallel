#include <iostream>
#include <vector>
#include <forward_list>
#include <shared_mutex>
using namespace std;


template <class Value, class Hash = hash<Value> >
class striped_hash_set{
private:
	const float tableLoadFactor;
	const size_t tableGrowthFactor;
	vector<forward_list<Value> > data;
	atomic <size_t> numOfElements;
	atomic <size_t> sizeOfTable;
	Hash hashFunction;
	vector<shared_timed_mutex> mutexes; //мы же можем их использовать?
public:
	striped_hash_set(size_t numOfStripes, size_t growthFactor = 2, float loadFactor = 3): tableLoadFactor(loadFactor), tableGrowthFactor(growthFactor), data(5 * numOfStripes),
		numOfElements(0), sizeOfTable(5 * numOfStripes), mutexes(numOfStripes) { ; }
	size_t get_stripe_index(size_t object) {
		return object % mutexes.size();
	}
	size_t get_bucket_index(size_t object) {
		return object % sizeOfTable.load();
	}
	bool contains(const Value& value) {
		size_t h = hashFunction(value);
		size_t indexOfStripe = get_stripe_index(h);
		unique_lock<shared_timed_mutex> lock(mutexes[indexOfStripe]); //захватили мьютекс, ответственный за данный страйп
		size_t indexOfBucket = get_bucket_index(h); // нашли корзину, в которой должен лежать этот элемент
		//forward_list<Value>::iterator element;
		for (auto element : data[indexOfBucket]) { // идем по корзине и ищем наш элемент
			if (element == value) {
				return true;
			}
		}
		return false;
	}
	void resize() { //добавили новый элемент, необходимо изменить размеры таблицы
		size_t newSize = sizeOfTable.load() * tableGrowthFactor;
		sizeOfTable.store(newSize);
		vector<forward_list<Value> > newData(sizeOfTable.load() * tableGrowthFactor);
		//forward_list<Value>::iterator element;
		for (auto basket : data) {
			for (auto element : basket) {
				size_t ind = get_bucket_index(hashFunction(element));
				newData[ind].push_front(element);
			}
		}
		swap(data, newData);
	}

	void add(const Value& value){
		if (contains(value)) { //проверяем, содержится ли элемент в таблице. если содержится, то засовывать его туда еще раз не будем
			return;
		}
		size_t sizeOfData = 0; //если его все же нет, то надо положить
		int hash = hashFunction(value);
		size_t indexOfStripe = get_stripe_index(hash); //определяем, в какой страйп засунуть элемент
		{ // нам нужно ненадолго захватить мьютекс, чтобы завладеть страйпом и, соответственно, корзиной
			unique_lock<shared_timed_mutex> lock(mutexes[indexOfStripe]); //блокируем соответствующий мьютекс
			size_t indexOfBucket = get_bucket_index(hash); //смотрим, в какую корзину засунуть
			data[indexOfBucket].push_front(value);//засовываем
			numOfElements.fetch_add(1);
			sizeOfData = sizeOfTable.load(); //теперь нужно проверить, не превышает ли количество элементов допустимое
		}
		double currentLoadFactor = numOfElements.load() / sizeOfData;
		if (currentLoadFactor > tableLoadFactor) { //нужно увеличить таблицу
			//если вдруг 2 потока решат одновременно расширить таблицу, то сделать это сможет только один
			vector<unique_lock<shared_timed_mutex> > vectorOfMutexesForLock;
			unique_lock<shared_timed_mutex> lock(mutexes[0]);
			if (sizeOfData != sizeOfTable.load()) { //проверяем, не расширил ли кто-то таблицу еще до того, как мы захватили мьютексы
				return;
			}
			for (size_t i = 1; i < mutexes.size(); i++) {
				vectorOfMutexesForLock.emplace_back(mutexes[i]);
			}
			resize();
		}
	}

	void remove(const Value& value) {
		if (contains(value)) { //проверяем, что такой элемент вообще есть, иначе удалять нечего
			int hash = hashFunction(value);
			size_t indexOfStripe = get_stripe_index(hash); //смотрим, в каком страйпе может лежать этот элемент
			unique_lock<shared_timed_mutex> lock(mutexes[indexOfStripe]); // захватываем соответствующий мьютекс
			size_t indexOfBucket = get_bucket_index(hash); // и в какой корзине
			data[indexOfBucket].remove(value); //удаляем соответствующий элемент
		}
	}
};