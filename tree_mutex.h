#include <thread>
#include <atomic>
#include <array>
#include <vector>
#include <stack>

using namespace std;

class peterson_mutex {
private:
	array<atomic<bool>, 2> want;
	atomic<int> victim;
public:
	peterson_mutex() {
		want[0].store(false);
		want[1].store(false);
		victim.store(0);
	}
	peterson_mutex(const peterson_mutex& other_mutex) {
		this->victim.store(other_mutex.victim.load());
		this->want[0].store(other_mutex.want[0].load());
		this->want[1].store(other_mutex.want[1].load());
	}
	void lock(int thread_index) {
		want[thread_index].store(true);
		victim.store(thread_index);
		while (want[1 - thread_index].load() && victim.load() == thread_index) {
			this_thread::yield();
		}
	}
	void unlock(int thread_index) {
		want[thread_index].store(false);
	}
};

class tree_mutex {
private:
	vector<peterson_mutex> peterson_mutex_tree;
	size_t size;
public:
	tree_mutex(size_t num_threads) {
		size = 2 * (1 << (int)(log2(num_threads) + 1));
		peterson_mutex_tree.resize(size);
	}
	void lock(size_t thread_index) {
		thread_index += size;
		size_t index = thread_index;
		while (index > 0) {
			size_t parent_index = index / 2;
			this->peterson_mutex_tree[parent_index].lock(index % 2);
			index /= 2;
		}
	}
	void unlock(size_t thread_index) {
		thread_index += size;
		stack<int> path;
		stack<int> which_son;
		size_t index = thread_index;
		while (index != 0) {
			which_son.push(index % 2);
			index /= 2;
			path.push(index);
		}
		while (!path.empty()) {
			peterson_mutex_tree[path.top()].unlock(which_son.top());
			path.pop();
			which_son.pop();
		}
	}
};
#pragma once
